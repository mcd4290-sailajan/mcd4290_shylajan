//question1
function task1(){
    var outputAreaRef = document.getElementById("outputArea1");
    var output = "";
    function flexible(fOperation, operand1, operand2){
        var result = fOperation(operand1, operand2);
        return result;
    }

    function sum(num1, num2) {
        return num1 + num2
    }

    function multiply(num1, num2) {
        return num1 * num2
    }
    output += flexible(sum, 3, 5) + "<br/>";
    output += flexible(multiply, 3, 5) + "<br/>";
    outputAreaRef.innerHTML = output;
}

function task2(){
    var outputAreaRef = document.getElementById("outputArea2");
    var output = "";
    var testObj = {
            number: 1,
            string: "abc",
            array: [5, 4, 3, 2, 1],
            boolean: true
        };
    
    var testObj2 = {
        name: "Bob",
        age: 32,
        married: true,
        children: ["Jack", "Jill"]
        };
    
    function objectToHTML(obj){
        var keys =  Object.keys(testObj);
        var objectText = "";
        for(var key of keys){
            objectText += key + " : " + testObj[key] + "<br>";
        }
        return objectText;
    }
    
    output += objectToHTML(testObj);
    
    output += "<br><br>";
    
    output += objectToHTML(testObj2);
    
    outputAreaRef.innerHTML = output;
}



//Question 2

function objectToHTML(obj){
let formattedString= "";
for(let iteration in obj){
    formattedString+= iteration+ ": "+ obj[iteration]+ "\n";
}
return formattedString;
}

const testObj = {
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
   };

   const outputString=objectToHTML(testObj);

   document.getElementById("outputArea1").innerText = outputString;

   //Question 3

   var outputAreaRef = document.getElementById("outputArea2");

var output = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}

function addTwoNumbers(num1,num2){
    return num1 + num2;
}

let multiplyTwoNumbers= (num1,num2)=> {
    return num1 * num2;
}

output += flexible(addTwoNumbers, 3, 5) + "<br/>";
output += flexible(multiplyTwoNumbers, 3, 5) + "<br/>"; 
outputAreaRef.innerHTML = output;



/*

1-The first step is to define a function that takes an array as an input(extreamValues).

2-Then we can define a variable for the current minimum value inside the function that we made
 and assign it to the first element of the given array.

 3-Similarly since we have to find the maximum value we need to define a variable for the current maximum
 value and also assign it to the first element of the array.

 4-Next step is to loop through the array that took as the input of the function. For loop through 
 the array we can give the initial iteration as 1 (since the first element is already checked ) 
 and the last iteration as the length of the array. 

 5-And we need to give a if statement that check whether the if current value is less than the current minimum.
 
 6-And also another if statement to check that if the current value is maximum than the current maximum. 

 7-Then after that if the current value is less than the minimum value we need to say assign that 
 new value to the variable that we made ealier for the current minimum value.

 8- Similarly we need to say if the current value is greater than the current maximum value we need to say 
 assign that new value to the variable that we made ealier for the current maximum value.
 

 9-After that end the loop and return the current minimum value and return the current maximum value.
         
 */


 //Question 4 

 function extreamValue(integers){
     let currentMinimumValue=integers[0];
     let currentMaximumValue=integers[0];
     for(let index=0; index<integers.length; index++){
         const element=integers[index];
         if(element<currentMinimumValue){
             currentMinimumValue=element;
         }
         if(element>currentMaximumValue){
             currentMaximumValue=element;
         }
     }

     return currentMaximumValue+ "\n" + currentMinimumValue;
 }

 var values = [4, 3, 6, 12, 1, 3, 8];

 document.getElementById("outputArea3").innerText=extreamValue(values);

   